/*
 * currenttest.cpp
   Software to test current using hand-built pcb
 
	Has a Max1243 Serial ADC
	* Pinout is
	*  Max1243			Rasp
	* 1   VDD			1 -- 3.3V
	* 2   AIN 			-- ladder with 10K ohm resistor and 1uF RC 
	* 3   SHUTDOWN#		-- No Connect
	* 4   REF			-- connect to 1 with 0.1uF decouple
	* 5   GND			30 -- GND
	* 6   DOUT			10
	* 7   CS#			6
	* 8   SCLK			5
	* 
	* It is like SPI, but it is not SPI so it must be bit-banged
	* 
	* Current +/- 130A mesured by TLI4971 in semi-differential mode

  */

#include <iostream>
#include <chrono>
#include <ctime>
#include <thread>
#include <cmath>
#include <wiringPi.h>

#include <stdio.h>
#include <sys/select.h>
#include <termios.h>
#include <stropts.h>
#include <unistd.h>
#include <sys/ioctl.h>


using namespace std;
using namespace std::this_thread; // sleep_for, sleep_until
using namespace std::chrono; // nanoseconds, system_clock, seconds

int ReadMax1243(void);
int _kbhit();

// convert ADC counts to volts
#define VOLTSPERCOUNT 0.00322265625
#define COUNTS2VOLTS(a)  (static_cast<double>(a) * VOLTSPERCOUNT)
#define BATTERYREAD(a)  ( (a - 1.60) / 0.01)

#define READSPEROUTPUT 1000 // read 1000 times


#define GPIO_DOUT		5 //24 //5
#define GPIO_CS			6 //25 // 6
#define GPIO_SCLK		10 //8 //4

int main( int argc, char ** argv )
{
		cout << "voltagetest -- reads the voltage approx 1000 times per second and outputs summary statistics" << endl;
		/////////// initializations
		// GPIOs
		wiringPiSetup();
		pinMode (GPIO_DOUT, INPUT) ;
		pinMode (GPIO_CS, OUTPUT) ;
		pinMode (GPIO_SCLK, OUTPUT) ;

		digitalWrite( GPIO_CS, HIGH );
		digitalWrite( GPIO_SCLK, LOW );
		
		auto timeStart = chrono::system_clock::now();
	
		// read the voltage every 1ms
		// output the max, min, and mean each second
		unsigned int  nVal;
		unsigned int nRead;
		unsigned int nMin, nMax;
		while(! _kbhit() )
		{
			nVal = 0; nMin = 2047; nMax = 0;
			for( int a = 0; a < READSPEROUTPUT; a++ )	
			{
				nRead = ReadMax1243();
				if( nRead < nMin )
					nMin = nRead;
				if( nRead > nMax )
					nMax = nRead;
				nVal += nRead;	
			}
			cout.setf(ios::fixed, ios::floatfield);
			cout.precision(2);
			//cout << "max = " << COUNTS2VOLTS(nMax) << " min = " << COUNTS2VOLTS(nMin) << " mean = " << COUNTS2VOLTS(nVal / READSPEROUTPUT) << endl;
			auto timenow = 
			chrono::system_clock::to_time_t(chrono::system_clock::now()); 
			cout << " Current, " << BATTERYREAD(COUNTS2VOLTS(nVal / READSPEROUTPUT)) << "," << ctime(&timenow);
		}	

		return 1;
}


/*
 * Reads out Max1432
 * receives 10 bit value
 * bit 1 is always high
 * bits 2-11 are the 10 bit value
 * bits 12-16 are always low
 * 
 */
int ReadMax1243(void)
{
	using namespace std::this_thread; // sleep_for, sleep_until
    using namespace std::chrono; // nanoseconds, system_clock, seconds

	int nRet = 0;
	// lower CS and wait 10uS for conversion
	digitalWrite( GPIO_CS, LOW );
    sleep_for(microseconds(20)); // wait 10us for sample and hold

	// clock out each bit of 16 bits
	unsigned int nBuf = 0;
	for( int a = 1; a < 16; a++ )
	{
		digitalWrite( GPIO_SCLK, HIGH );
		sleep_for(microseconds(20)); // wait 10us
		nBuf = digitalRead( GPIO_DOUT );
		if( a > 1 && a <12 ) 
			nRet = nRet + (nBuf << 12 - a );
		nBuf = 0;
		digitalWrite( GPIO_SCLK, LOW );
		sleep_for(microseconds(20)); // wait 10us
	}

	// clean-up
	digitalWrite( GPIO_CS, HIGH );
	
	return nRet;	
}


int _kbhit() {
    static const int STDIN = 0;
    static bool initialized = false;

    if (! initialized) {
        // Use termios to turn off line buffering
        termios term;
        tcgetattr(STDIN, &term);
        term.c_lflag &= ~ICANON;
        tcsetattr(STDIN, TCSANOW, &term);
        setbuf(stdin, NULL);
        initialized = true;
    }

    int bytesWaiting;
    ioctl(STDIN, FIONREAD, &bytesWaiting);
    return bytesWaiting;
}

