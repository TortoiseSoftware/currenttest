currenttest uses a MAX1243 ADC and a Raspberry Pi to read the current at the ADC.
the current is sampled with Infineon TLI4971 in semi-differential mode.
currenttest takes 1000 samples in a row (about 1.5 ms per sample) and outputs the average.
The format is CSV.
The first field is clock time, and the second is the value of the current in amperes.

A good use is to ./currentteset | nc <host> <port> to output the results via tcp.
The corresponding server would just be nc -l <port>.

Another good use might be ./currentteset > logfile.csv .

To terminate the program cleanly, ENTER.
